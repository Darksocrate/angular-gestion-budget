import { Component, OnInit } from '@angular/core';
import { OperationsService } from '../services/operations.service';
import { Operations } from '../entity/operations';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent {

  operations: Operations[] = [];

  newOperation: Operations = { credit: 0, debit: 0, date: "", description: "" };


  constructor(private service: OperationsService) {

  }

  ngOnInit() {
    this.service.findAll().subscribe(response => {
      this.operations = response;

      let sommecre = 0;
      let sommedeb = 0;

      for (let operation of this.operations) {
        sommecre = operation.credit + sommecre;
        sommedeb = operation.debit + sommedeb;

      }
      this.pieChartData[1] = sommecre;
      this.pieChartData[0] = sommedeb;
      

    });
  }

  // Pie
  public pieChartLabels: string[] = ['Debit', 'Credit'];
  public pieChartData: number[] = [ , ];
  public pieChartType: string = 'pie';
  public chartColors: any[] = [
    {
      backgroundColor: ["#BB2C3B", "#232C4A"]
    }];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

}