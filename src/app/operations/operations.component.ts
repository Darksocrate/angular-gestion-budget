import { Component, OnInit } from '@angular/core';
import { Operations } from '../entity/operations';
import { OperationsService } from '../services/operations.service';

@Component({
  selector: 'add-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {

  operations: Operations[] = [];

  newOperation: Operations = { credit: 0 , debit: 0, date: "", description: "" };

  selected: Operations;

  total:number;

  constructor(private service: OperationsService) { }

  ngOnInit() {
    this.service.findAll().subscribe(response => {
      this.operations = response;
     
      let somme = 0;
      
      for(let operation of this.operations) {
     somme = -operation.debit+operation.credit+somme;
      }
      this.total = somme;
    });
  }

  addOperation() {
    this.service.add(this.newOperation).subscribe(() => { this.ngOnInit();}); 
  }

  deleteOperation(selected) {
    this.service.delete(selected.id).subscribe(() => { this.ngOnInit();});
  }


}
