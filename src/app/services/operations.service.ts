import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Operations } from '../entity/operations';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OperationsService {

  private url = 'http://localhost:8080';

  number1 = null;
  number2 = null;
  total: number;

  constructor(private http: HttpClient) { }

  findAll(): Observable<Operations[]> {
    return this.http.get<Operations[]>(this.url).pipe(map(data => {
      for (let operations of data) {
        operations.date = new Date(operations.date);
      }
      return data;
    }));
  }

  add(operations: Operations): Observable<Operations> {

    operations.date = new Date(operations.date).getTime();

    return this.http.post<Operations>(`${this.url}/add`, operations);

  }

  update(operations: Operations): Observable<Operations> {
    return this.http.put<Operations>(this.url, operations);

  }

  delete(id: number): Observable<Operations> {
    return this.http.delete<Operations>(this.url + "/" + id);

  }

  addition(total) {
    this.total = this.number1 + this.number2;
    return (total);
  }
}
