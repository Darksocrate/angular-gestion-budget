export interface Operations {
  id?:number;
  debit:number;
  credit:number;
  date: any;
  description: string;
}
